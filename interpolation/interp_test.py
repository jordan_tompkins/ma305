#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Create easier urls for latexrefman tree view
"""
__version__ = "0.7.5"
__author__ = "Jordan Tompkins"
__license__ = "GPL3"


import sys
import os
import traceback
import argparse
import time
import unittest
import numpy as np
import interp
# Global variables spare me from putting them in the call of each fcn.
VERBOSE = False
DEBUG = False

class JHException(Exception):
    pass

def warn(s):
    t = 'WARNING: '+s+"\n"
    sys.stderr.write(t)
    sys.stderr.flush()

def error(s):
    t = 'ERROR! '+s
    sys.stderr.write(t)
    sys.stderr.flush()
    sys.exit(10)

import logging
# create file handler which logs even debug messages
log = logging.getLogger()
# Potential logging levels: DEBUG | INFO | WARNING | ERROR | CRITICAL
if DEBUG:
    log.setLevel(logging.DEBUG)  
else:
    log.setLevel(logging.ERROR)  # DEBUG | INFO | WARNING | ERROR | CRITICAL
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - Line: %(lineno)d\n%(message)s')
sh = logging.StreamHandler()
sh.setLevel(logging.ERROR)
sh.setFormatter(formatter)
log.addHandler(sh)
fh = logging.FileHandler(os.path.abspath(os.path.join(
    os.path.dirname(__file__), 
    os.path.basename(__file__).rstrip('.py') + '.log'
)))
fh.setLevel(logging.ERROR)
fh.setFormatter(formatter)
log.addHandler(fh)

# ==============================================
class PLinearTestCase(unittest.TestCase):
    """Tests."""
    import numpy as np
    
    def test_simple(self):
        """Do simple case of ([1,2], [3,4], [5,6])"""
        list_of_pts = np.array([[1,2], [3,4], [5,6]])
        y = interp.p_linear(list_of_pts, 2)
        self.assertEqual(y,3)
    def test_frac_numbers(self):
        """Do a test of fraction  values"""
        list_of_pts = np.array([[1/2,3/4], [5/4,6/4], [7/4,8/4]])
        y = interp.p_linear(list_of_pts, 1/2)
        self.assertEqual(y, .75)
    def test_neg_numbers(self):
        """Do a test of negative values"""
        list_of_pts = [(-1,-2), (-3,-4), (-5,-6)]
        y = interp.p_linear(list_of_pts, -2)
        self.assertEqual(y,-3)
    def test_unsorted_numbers(self):
        """Do a test where numbers are unsorted"""
        list_of_pts = [(7,8), (5,6), (9,10)]
        y =interp.p_linear(list_of_pts, 7)
        self.assertEqual(y, 8)
    def test_duplicates(self):
        """Do a test where there are duplicate x values"""
        list_of_pts = np.array([[1,2], [1,4], [5,6]])
        y = interp.p_linear(list_of_pts, 2)
        self.assertIsNone(y)
    def test_out_of_bounds(self):
        """Do a test where the range is out of bounds"""
        list_of_pts = np.array([[1,2], [3,4], [5,6]])
        y = interp.p_linear(list_of_pts, 7)
        self.assertIsNone(y)
# ===========================================================
def main(args):
    unittest.main()

# ===========================================================
if __name__ == '__main__':
    try:
        start_time = time.time()
        # Parser: See http://docs.python.org/dev/library/argparse.html
        parser = argparse.ArgumentParser(description=__doc__+
                                         "\n"+__author__
                                         +" version: "+__version__
                                         +" license: "+__license__)
        parser.add_argument('-D', '--debug', action='store_true', default=DEBUG, help='run debugging code')
        parser.add_argument('-v', '--version', action='version', version=__version__)
        parser.add_argument('-V', '--verbose', action='store_true', default=False, help='verbose output')
        args = parser.parse_args()
        if args.debug:
            fh.setLevel(logging.DEBUG)
            log.setLevel(logging.DEBUG)
        elif args.verbose:
            fh.setLevel(logging.INFO)
            log.setLevel(logging.INFO)
        log.info("%s Started" % parser.prog)
        main(args)
        log.info("%s Ended" % parser.prog)
        log.info("Total running time in seconds: %0.2f" % (time.time() - start_time))
        sys.exit(0)
    except KeyboardInterrupt as e:  # Ctrl-C
        raise e
    except SystemExit as e:  # sys.exit()
        raise e
    except Exception as e:
        print('ERROR, UNEXPECTED EXCEPTION')
        print(str(e))
        traceback.print_exc()
        os._exit(1)

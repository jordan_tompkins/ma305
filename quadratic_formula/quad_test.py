#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Create easier urls for latexrefman tree view
"""
# from _version import __version__, __author__, __license__

__version__ = "0.0.1"
__author__ = "Jim Hefferon"
__license__ = "GPL 3.0"

import sys
import os
import traceback
import argparse
import time
import unittest
import quad 

# Global variables spare me from putting them in the call of each fcn.
VERBOSE = False
DEBUG = False

class JHException(Exception):
    pass

def warn(s):
    t = 'WARNING: '+s+"\n"
    sys.stderr.write(t)
    sys.stderr.flush()

def error(s):
    t = 'ERROR! '+s
    sys.stderr.write(t)
    sys.stderr.flush()
    sys.exit(10)

import logging
# create file handler which logs even debug messages
log = logging.getLogger()
# Potential logging levels: DEBUG | INFO | WARNING | ERROR | CRITICAL
if DEBUG:
    log.setLevel(logging.DEBUG)  
else:
    log.setLevel(logging.ERROR)  # DEBUG | INFO | WARNING | ERROR | CRITICAL
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - Line: %(lineno)d\n%(message)s')
sh = logging.StreamHandler()
sh.setLevel(logging.ERROR)
sh.setFormatter(formatter)
log.addHandler(sh)
fh = logging.FileHandler(os.path.abspath(os.path.join(
    os.path.dirname(__file__), 
    os.path.basename(__file__).rstrip('.py') + '.log'
)))
fh.setLevel(logging.ERROR)
fh.setFormatter(formatter)
log.addHandler(fh)

# ==============================================
class QuadraticTestCase(unittest.TestCase):
    """Tests."""

    def test_simple(self):
        """Do the dumbest possible thing"""
        r1,r2 = -3.0, -3.0
        self.assertEqual(quad.quad_formula(1,6,9), (r1,r2))

    def test_quad_one(self):
        """Test the program for normal calculations"""
        r1,r2 = -3.0, -3.0
        self.assertEqual(quad.driver(1,6,9), (r1,r2))
    def test_quad_two(self):
        """Test the program for a and b equal 0 leaving r2 as an empty string"""
        r1,r2 = 7,''
        self.assertEqual(quad.driver(0,0,7), (r1,r2))
    def test_quad_three(self):
        """Test program for a equal to 0 but b and c have values, leaving r2 as empty string"""
        r1,r2 = 1.25, ''
        self.assertEqual(quad.driver(0,4,-5), (r1,r2))
    def test_quad_four(self):
        """Test program for square root of discriminant very close to b"""
        r1,r2 = 1971.605915932872, 0.050770693874568965
        self.assertEqual(quad.driver(0.05010,-98.78,5.015), (r1,r2))
    def test_quad_five(self):
        """Test program for b minus square root of discriminant being very close to 0"""
        r1,r2= 395.9949494305347, 0.005050569465282706
        self.assertEqual(quad.driver(0.5, -198.0, 1.0), (r1,r2))
     

# ===========================================================
def main(args):
    unittest.main()

# ===========================================================
if __name__ == '__main__':
    try:
        start_time = time.time()
        # Parser: See http://docs.python.org/dev/library/argparse.html
        parser = argparse.ArgumentParser(description=__doc__+
                                         "\n"+__author__
                                         +" version: "+__version__
                                         +" license: "+__license__)
        parser.add_argument('-D', '--debug', action='store_true', default=DEBUG, help='run debugging code')
        parser.add_argument('-v', '--version', action='version', version=__version__)
        parser.add_argument('-V', '--verbose', action='store_true', default=False, help='verbose output')
        args = parser.parse_args()
        if args.debug:
            fh.setLevel(logging.DEBUG)
            log.setLevel(logging.DEBUG)
        elif args.verbose:
            fh.setLevel(logging.INFO)
            log.setLevel(logging.INFO)
        log.info("%s Started" % parser.prog)
        main(args)
        log.info("%s Ended" % parser.prog)
        log.info("Total running time in seconds: %0.2f" % (time.time() - start_time))
        sys.exit(0)
    except KeyboardInterrupt as e:  # Ctrl-C
        raise e
    except SystemExit as e:  # sys.exit()
        raise e
    except Exception as e:
        print('ERROR, UNEXPECTED EXCEPTION')
        print(str(e))
        traceback.print_exc()
        os._exit(1)
